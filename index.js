function sortNumber() {
  var large,
    mid,
    small = 0;
  var num1 = +document.getElementById("inputNum1").value;
  var num2 = +document.getElementById("inputNum2").value;
  var num3 = +document.getElementById("inputNum3").value;
  if (num1 > num2 && num1 > num3) {
    large = num1;
    mid = num2;
    small = num3;
  } else if (num1 > num2 && num3 > num2) {
    large = num1;
    mid = num3;
    small = num2;
  } else if (num1 > num2 && num3 > num1) {
    large = num3;
    mid = num1;
    small = num2;
  } else if (num2 > num1 && num2 > num3) {
    large = num2;
    mid = num1;
    small = num3;
  } else if (num2 > num3 && num3 > num1) {
    large = num2;
    mid = num3;
    small = num1;
  } else {
    large = num3;
    mid = num2;
    small = num1;
  }
  document.getElementById(
    "txtSortNumber"
  ).innerHTML = ` Sắp xếp : ${large} > ${mid} > ${small}`;
}
function sayHello() {
  var who = document.getElementById("selUser").value;
  var hello = "";
  if (who == "B") {
    hello = "Xin chào Bố";
  } else if (who == "M") {
    hello = "Xin chào Mẹ";
  } else if (who == "A") {
    hello = "Xin chào Anh trai";
  } else if (who == "E") {
    hello = "Xin chào Em gái";
  } else {
    hello = "Xin chào người lạ ơi";
  }
  document.getElementById("txtHello").innerHTML = `${hello}`;
}
function countNumber() {
  var number1 = +document.getElementById("inputCount1").value;
  var number2 = +document.getElementById("inputCount2").value;
  var number3 = +document.getElementById("inputCount3").value;
  var count = 0;

  if (number1 % 2 == 0) {
    count++;
  }
  if (number2 % 2 == 0) {
    count++;
  }
  if (number3 % 2 == 0) {
    count++;
  }
  var countLeft = 3 - count;
  document.getElementById(
    "txtCount"
  ).innerHTML = ` Có ${count} số chẵn, ${countLeft} số lẻ `;
}
function foundTriangle() {
  var edge1 = +document.getElementById("inputEdge1").value;
  var edge2 = +document.getElementById("inputEdge2").value;
  var edge3 = +document.getElementById("inputEdge3").value;
  var powEdge1 = Math.pow(edge1, 2);
  var powEdge2 = Math.pow(edge2, 2);
  var powEdge3 = Math.pow(edge3, 2);
  var sumE12 = edge1 + edge2;
  var sumE13 = edge1 + edge3;
  var sumE23 = edge2 + edge3;
  var type = "";
  if (edge1 == edge2 && edge1 == edge3) {
    type = "Tam giác đều";
  } else if (edge1 == edge2 || edge1 == edge3 || edge2 == edge3) {
    type = "Tam giác cân";
  } else if (
    powEdge1 == powEdge2 + powEdge3 ||
    powEdge2 == powEdge1 + powEdge3 ||
    powEdge3 == powEdge1 + powEdge2
  ) {
    type = "Tam giác vuông";
  } else if (sumE12 > edge3 && sumE13 > edge2 && sumE23 > edge3) {
    type = "Tam giác thường";
  } else {
    type =
      "Không phải tam giác (tổng 2 cạnh bất kì trong một tam giác luôn lớn hơn cạnh còn lại)";
  }
  document.getElementById("txtEdge").innerHTML = `${type}`;
}
// tính ngày /////////////////////////////////////////////
// tomorrow /////////////////////////////////////////////
function calTomorrow() {
  var day = +document.getElementById("inputDay").value;
  var month = +document.getElementById("inputMonth").value;
  var year = +document.getElementById("inputYear").value;
  var tomorrow = "";
  switch (month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
      if (day == 31) {
        month += 1;
        day = 1;
        tomorrow = day + "/" + month + "/" + year;
      } else if (0 < day && day < 31) {
        day += 1;
        tomorrow = day + "/" + month + "/" + year;
      } else {
        tomorrow = "Ngày không xác định";
      }
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      if (day == 30) {
        month += 1;
        day = 1;
        tomorrow = day + "/" + month + "/" + year;
      } else if (0 < day && day < 30) {
        day += 1;
        tomorrow = day + "/" + month + "/" + year;
      } else {
        tomorrow = "Ngày không xác định";
      }
      break;
    case 2:
      if (day == 28) {
        month += 1;
        day = 1;
        tomorrow = day + "/" + month + "/" + year;
      } else if (0 < day && day < 28) {
        day += 1;
        tomorrow = day + "/" + month + "/" + year;
      } else {
        tomorrow = "Ngày không xác định";
      }
      break;
    case 12:
      if (day == 31) {
        month = 1;
        day = 1;
        year += 1;
        tomorrow = day + "/" + month + "/" + year;
      } else if (0 < day && day < 31) {
        day += 1;
        tomorrow = day + "/" + month + "/" + year;
      } else {
        tomorrow = "Ngày không xác định";
      }
    default:
      tomorrow = "Tháng không xác định";
  }
  document.getElementById("txtDate").innerHTML = `${tomorrow}`;
}
// yesterday /////////////////////////////////////////////

function calYesterday() {
  var day = +document.getElementById("inputDay").value;
  var month = +document.getElementById("inputMonth").value;
  var year = +document.getElementById("inputYear").value;
  var yesterday = "";
  switch (month) {
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      if (day == 1) {
        month -= 1;
        day = 30;
        yesterday = day + "/" + month + "/" + year;
      } else if (0 < day && day < 31) {
        day -= 1;
        yesterday = day + "/" + month + "/" + year;
      } else {
        yesterday = "Ngày không xác định";
      }
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      if (day == 1) {
        month -= 1;
        day = 31;
        yesterday = day + "/" + month + "/" + year;
      } else if (0 < day && day < 30) {
        day -= 1;
        yesterday = day + "/" + month + "/" + year;
      } else {
        yesterday = "Ngày không xác định";
      }
      break;
    case 2:
      if (day == 1) {
        month = 1;
        day = 1;
        yesterday = day + "/" + month + "/" + year;
      } else if (0 < day && day < 28) {
        day -= 1;
        yesterday = day + "/" + month + "/" + year;
      } else {
        yesterday = "Ngày không xác định";
      }
      break;
    case 1:
      if (day == 1) {
        month = 12;
        day = 31;
        year -= 1;
        yesterday = day + "/" + month + "/" + year;
      } else if (0 < day && day < 31) {
        day -= 1;
        yesterday = day + "/" + month + "/" + year;
      } else {
        yesterday = "Ngày không xác định";
      }
      break;
    default:
      yesterday = "Tháng không xác định";
  }
  document.getElementById("txtDate").innerHTML = `${yesterday}`;
}
// tính ngày trong tháng
function calDay() {
  var monthInput = +document.getElementById("inputMonth2").value;
  var yearInput = +document.getElementById("inputYear2").value;
  var dayOutput;
  switch (monthInput) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      dayOutput = 31;
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      dayOutput = 30;
      break;
    case 2:
      if (
        (yearInput % 4 == 0 && yearInput % 100 !== 0) ||
        yearInput % 400 == 0
      ) {
        dayOutput = 29;
      } else {
        dayOutput = 28;
      }
      break;
    default:
      dayOutput = 0;
  }
  document.getElementById(
    "txtCalcDay"
  ).innerHTML = ` Tháng ${monthInput} năm ${yearInput} có ${dayOutput} ngày `;
}
// đọc số có 3 chữ số
function readnumber() {
  var numToRead = +document.getElementById("inputReadInt").value;
  var hundreds = Math.floor(numToRead / 100);
  var tens = Math.floor((numToRead % 100) / 10);
  var units = Math.floor((numToRead % 100) % 10);
  console.log("units: ", units);

  var nameH = "";
  var nameT = "";
  var nameU = "";
  switch (hundreds) {
    case 1:
      nameH = "Một trăm";
      break;
    case 2:
      nameH = "Hai trăm";
      break;
    case 3:
      nameH = "Ba trăm";
      break;
    case 4:
      nameH = "Bốn trăm";
      break;
    case 5:
      nameH = "Năm trăm";
      break;
    case 6:
      nameH = "Sáu trăm";
      break;
    case 7:
      nameH = "Bảy trăm";
      break;
    case 8:
      nameH = "Tám trăm";
      break;
    case 9:
      nameH = "Chín trăm";
      break;
  }
  switch (tens) {
    case 0:
      nameT = "lẻ";
      break;
    case 1:
      nameT = "mười";
      break;
    case 2:
      nameT = "hai mươi";
      break;
    case 3:
      nameT = "ba mươi";
      break;
    case 4:
      nameT = "bốn mươi";
      break;
    case 5:
      nameT = "năm mươi";
      break;
    case 6:
      nameT = "sáu mươi";
      break;
    case 7:
      nameT = "bảy mươi";
      break;
    case 8:
      nameT = "tám mươi";
      break;
    case 9:
      nameT = "chín mươi";
      break;
    default:
      nameT = "";
  }
  switch (units) {
    case 0:
      if (tens == 0) {
        nameT = "";
        nameU = "";
      } else {
        nameU = "";
      }
      break;
    case 1:
      nameU = "một";
      break;
    case 2:
      nameU = "hai";
      break;
    case 3:
      nameU = "ba";
      break;
    case 4:
      nameU = "bốn";
      break;
    case 5:
      nameU = "năm";
      break;
    case 6:
      nameU = "sáu";
      break;
    case 7:
      nameU = "bảy";
      break;
    case 8:
      nameU = "tám";
      break;
    case 9:
      nameU = "chín";
      break;
  }
  document.getElementById(
    "txtReadInt"
  ).innerHTML = `${nameH} ${nameT} ${nameU}`;
}
// tính sinh viên xa trường theo tọa độ
function foundFurther() {
  var name1 = document.getElementById("inputName1").value;
  var name2 = document.getElementById("inputName2").value;
  var name3 = document.getElementById("inputName3").value;
  var nameStudent = "";
  // tọa độ nhà sv 1
  var x1 = +document.getElementById("inputX1").value;
  var y1 = +document.getElementById("inputY1").value;
  // tọa độ nhà sv 2
  var x2 = +document.getElementById("inputX2").value;
  var y2 = +document.getElementById("inputY2").value;
  // tọa độ nhà sv 3
  var x3 = +document.getElementById("inputX3").value;
  var y3 = +document.getElementById("inputY3").value;
  // tọa độ trường
  var x4 = +document.getElementById("inputX4").value;
  var y4 = +document.getElementById("inputY4").value;
  // khoảng cách nhà từng sv tới trường
  var dXY1 = Math.sqrt(Math.pow(x1 - x4, 2) + Math.pow(y1 - y4, 2));
  var dXY2 = Math.sqrt(Math.pow(x2 - x4, 2) + Math.pow(y2 - y4, 2));
  var dXY3 = Math.sqrt(Math.pow(x3 - x4, 2) + Math.pow(y3 - y4, 2));
  // so sánh
  if (dXY1 > dXY2 && dXY1 > dXY3) {
    nameStudent = name1;
  } else if (dXY2 > dXY1 && dXY2 > dXY3) {
    nameStudent = name2;
  } else {
    nameStudent = name3;
  }
  document.getElementById(
    "txtSearch"
  ).innerHTML = `Sinh viên xa trường nhất ${nameStudent}`;
}
